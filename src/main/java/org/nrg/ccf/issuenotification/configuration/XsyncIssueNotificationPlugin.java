package org.nrg.ccf.issuenotification.configuration;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

/**
 * The Class XsyncIssueNotificationPlugin.
 */
@XnatPlugin(
				value = "xsyncIssueNotificationPlugin",
				name = "XSync Issue Notification Plugin"
			)
@ComponentScan({
	"org.nrg.ccf.issuenotification.event.listeners"
	})
public class XsyncIssueNotificationPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(XsyncIssueNotificationPlugin.class);

	/**
	 * Instantiates a new ccf subject ids plugin.
	 */
	public XsyncIssueNotificationPlugin() {
		logger.info("Configuring XSync Issue Notification plugin");
	}
}
