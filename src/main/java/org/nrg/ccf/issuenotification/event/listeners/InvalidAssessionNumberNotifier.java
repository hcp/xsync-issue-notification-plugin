package org.nrg.ccf.issuenotification.event.listeners;

import org.nrg.xdat.turbine.utils.AdminUtils;
import reactor.bus.Event;
import reactor.bus.EventBus;
import reactor.fn.Consumer;
//import org.nrg.ccf.issuenotification.exception.SubjectIdsFormatException;
//import org.nrg.ccf.issuenotification.exception.SubjectIdsNonUniqueException;
//import org.nrg.ccf.issuenotification.utils.SubjectIdsUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xft.event.entities.WorkflowStatusEvent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.springframework.stereotype.Service;
import static reactor.bus.selector.Selectors.R;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

@Service
public class InvalidAssessionNumberNotifier implements Consumer<Event<WorkflowStatusEvent>> {
	
	final static Map<String,Long> callCache = new HashMap<>();
	// TODO:  Make this configurable
	final String[] emailList = new String[] { "hodgem@wustl.edu", "akaushal@wustl.edu" };
	
    @Inject
    public InvalidAssessionNumberNotifier(final EventBus eventBus) {
		eventBus.on(R(WorkflowStatusEvent.class.getName() + "[.]?(" + PersistentWorkflowUtils.COMPLETE + "|" + PersistentWorkflowUtils.FAILED + ")"), this);
    }

    @Override
    public void accept(final Event<WorkflowStatusEvent> event) {

        final WorkflowStatusEvent wfsEvent = event.getData();
        if (wfsEvent.getWorkflow() instanceof WrkWorkflowdata) {
            handleEvent(wfsEvent);
        }

    }

	private void handleEvent(final WorkflowStatusEvent wfsEvent) {
		final WrkWorkflowdata workflow = (WrkWorkflowdata)wfsEvent.getWorkflow();
		final String id = workflow.getId();
		final String externalId = workflow.getExternalid();
		final String dataType = workflow.getDataType();
		if (dataType.equals("xnat:experimentData") || 
			dataType.equals("xnat:mrSessionData") || 
			dataType.equals("xnat:imageSessionData")) {
			if (evalCache(id) && id.matches("^.*_S[0-9]*$")) {
				final String message = "The subject assession number <b>" + id + "</b> has been assigned to " +
						"an experiment in project <b>" + externalId + "</b>.";
				final String msgSubject = XDAT.getSiteConfigPreferences().getSiteId() + 
						" ERROR: Invalid assession number for experiment " + workflow.getId();
				if (AdminUtils.sendUserHTMLEmail(msgSubject, message, false, emailList)) {
					callCache.put(id, System.currentTimeMillis());
				}
			}
		} else if (workflow.getType().equals("xnat:subjectData")) {
			if (evalCache(id) && id.matches("^.*_E[0-9]*$")) {
				final String message = "The experiment assession number <b>" + id + "</b> has been assigned to " +
						"a subject in project <b>" + externalId + "</b>.";
				final String msgSubject = XDAT.getSiteConfigPreferences().getSiteId() + 
						" ERROR: Invalid assession number for experiment " + workflow.getId();
				if (AdminUtils.sendUserHTMLEmail(msgSubject, message, false, emailList)) {
					callCache.put(id, System.currentTimeMillis());
				}
			}
		}
		
	}

	private boolean evalCache(final String id) {
		// Only send one e-mail per hour per ID.
		if (callCache.containsKey(id)) {
			if (System.currentTimeMillis()-callCache.get(id) < (1000*60*60)) {
				return false;
			}
		}
		return true;
	}

}
